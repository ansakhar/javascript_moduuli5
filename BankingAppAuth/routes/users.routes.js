import express from "express";
import authRouter from "../controllers/auth.controllers.js";
import * as UserController from "../controllers/users.controllers.js";

const router = express.Router();

router.post("/user", UserController.postUser); // 1
router.get("/:user_id/balance", UserController.getUser); // 2
router.post("/login", UserController.login); // 7
router.use("/", authRouter);
authRouter.patch("/user/withdraw", UserController.withdrawMoney); // 3
authRouter.patch("/user/deposit", UserController.depositMoney); // 4
authRouter.post("/transfer", UserController.transferMoney); // 5
authRouter.patch("/update", UserController.updateUsername); // 6
authRouter.patch("/user/password", UserController.updatePassword); // 6B
authRouter.post("/request_funds", UserController.requestFunds); // 8
authRouter.post("/transfer_by_request", UserController.transferByRequest); // 9
/*router.get("/", UserController.getAllUsers);
router.delete("/:id", UserController.deleteUser);*/

export default router;