import express from "express";
import jwt from "jsonwebtoken";
import dotenv from "dotenv";

dotenv.config(); // access environment variables
const jwtKey = process.env.APP_SECRET;

const authRouter = express.Router();

authRouter.use((req, res, next) => {
    const authorization = req.get("authorization");
    if (authorization && authorization.toLowerCase().startsWith("bearer ")) {
        const token = authorization.substring(7);
        const decodedToken = jwt.verify(token, jwtKey);
        //console.log("token:", decodedToken);
        if(!token || !decodedToken.userId) { // ?_id?
            return res.status(401).json({error: "token missing or invalid"});
        }
        req.user = decodedToken;
        next();
    } else {
        return res.status(401).json({error: "token missing or invalid"});
    }
});

export default authRouter;