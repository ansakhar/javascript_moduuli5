import express from "express";
import { insertCars } from "./controllers/cars.controllers.js";
import carRouter from "./routes/cars.routes.js";
import mongoose from "mongoose";
import dotenv from "dotenv";

dotenv.config();

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));

mongoose.connect("mongodb+srv://fullstackas:143080Mongo@cluster0.mrl4q.mongodb.net/carsDB", {useNewUrlParser: true, useUnifiedTopology: true});

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function() {
    console.log("Database connected");
});

app.use("/", carRouter);

insertCars();

app.listen(5000, () => {
    console.log("Listening to 5000");
});
