import express from "express";
import mongoose from "mongoose";
import dotenv from "dotenv";
import studentRouter from "./routes/students.routes.js";

dotenv.config();

async function connectMongoose() {
    try {
        await mongoose.connect(
            "mongodb://localhost/minuntietokanta",
            { 
                useNewUrlParser: true,
                useUnifiedTopology: true,
                user: process.env.MONGODB_USER,
                pass: process.env.MONGODB_PASS
            }
        );
    } catch (err) {
        console.log(err);
    }
}

export const app = express();
connectMongoose();

app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use("/students", studentRouter);

app.listen(5000, () => {
    console.log("Listening to 5000");
});
