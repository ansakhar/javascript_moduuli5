import mongoose from "mongoose";

const StudentSchema = new mongoose.Schema({
    name: String,
    class: String
});

const Student = mongoose.model("Students", StudentSchema);
export default Student;