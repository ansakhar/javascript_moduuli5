import express from "express";
import * as CarController from "../controllers/cars.controllers.js";

const router = express.Router();

// Student includes id, email and name
router.get("/cars", CarController.getAllCars);
router.get("/usedCar/:id", CarController.getUsedCar);
router.post("/usedCar/", CarController.postUsedCar);
/*router.put("/:id", StudentController.putStudent);
router.delete("/:id", StudentController.deleteStudent);*/

export default router;