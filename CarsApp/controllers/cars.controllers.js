import Car from "../models/car.model.js";
import fs from "fs";

let cars = [];

function readJsonFile() {
    try {
        cars = JSON.parse(fs.readFileSync("user_cars.json", "utf-8"));
    } catch {
        cars = [];
    }
}

export function insertCars() { 
    readJsonFile();
    Car.insertMany(cars, {});
}

export const getAllCars = async (request, response) => {
    const cars = await Car.find({});
    response.json(cars);
};

// GET /usedCar/{id} → Get all the information about a user car via its ID
export const getUsedCar = async (req, res) => {
    try {
        const user = await Car.findById(req.params.id);
        if (user !== null) res.send(user);
        else return res.status(404).json({error: "User not found"});
    } catch (err) {
        res.status(500).send(err);
    }
};

// POST /usedCar → Store used car information into our database and return the ID.
export const postUsedCar = async (req, res) => {
    const car = new Car();
    const fields = Object.keys(Car.schema.tree);
    console.log("fields", fields);
    fields.forEach(field => {
        car[field] = req.body[field];
    });
    const savedCar = await car.save();
    res.status(200).json(savedCar._id);
};

/*export const postUserCarr = async (req, res) => {
    try {
        console.log(req.body);
        const user = new Car({
            owner_firstname: req.body.owner_firstname,
            owner_surname: req.body.owner_surname
        });
  
        const savedUser = await user.save();
        res.json(savedUser);
    }
    catch (error) {
        console.log(error.message);
    }
};*/

/*
export const putStudent = async (req, res, next) => {
    const body = req.body;
    try {
        const updatedStudent = await Student.findByIdAndUpdate(req.params.id, body, { new: true });
        if (updatedStudent) res.json(updatedStudent);
        else res.status(404).json({error: "User not found"});
    } catch(error) {
        next(error);
    }
};

export const deleteStudent = async (req, res) => {
    const student = await Student.findOneAndDelete({id:req.params.id});
    console.log("student", student);
    if (student !== null) res.json(student);
    else res.status(404).json({error: "User not found"});
};*/
/*
export const deleteStudent = (req, res) => {
    Student.findByIdAndRemove(req.params.id)
        .then(() => {
            res.status(200).send("Student deleted");
        })
        .catch(err => {
            res.status(500).send(err);
        });
};*/