import Student from "../models/students.model.js";

export const getAllStudents = async (request, response) => {
    const students = await Student.find({});
    response.json(students);
};

export const getStudent = async (req, res) => {
    try {
        const student = await Student.findById(req.params.id);

        if (student !== null) res.send(student);
        return res.status(404).json({error: "User not found"});
    } catch (err) {
        res.status(500).send(err);
    }
};

export const postStudent = async (req, res) => {
    const student = new Student();
    const fields = ["firstname", "surname", "birthday", "email"];
    fields.forEach(field => {
        student[field] = req.body[field];
    });
    await student.save();
    res.status(200).send("Success");
};

export const putStudent = async (req, res, next) => {
    const body = req.body;
    try {
        const updatedStudent = await Student.findByIdAndUpdate(req.params.id, body, { new: true });
        if (updatedStudent) res.json(updatedStudent);
        else res.status(404).json({error: "User not found"});
    } catch(error) {
        next(error);
    }
};

export const deleteStudent = async (req, res) => {
    const student = await Student.findOneAndDelete({id:req.params.id});
    console.log("student", student);
    if (student !== null) res.json(student);
    else res.status(404).json({error: "User not found"});
};
/*
export const deleteStudent = (req, res) => {
    Student.findByIdAndRemove(req.params.id)
        .then(() => {
            res.status(200).send("Student deleted");
        })
        .catch(err => {
            res.status(500).send(err);
        });
};*/