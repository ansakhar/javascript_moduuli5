import express from "express";
//import { readJsonFile } from "./controllers/students.controllers.js";
import studentRouter from "./routes/students.routes.js";
import mongoose from "mongoose";
import dotenv from "dotenv";

dotenv.config();

const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));

mongoose.connect(process.env.MONGODB_URI_STUDENTS, {useNewUrlParser: true, useUnifiedTopology: true});

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function() {
    console.log("Database connected");
});

app.use("/students", studentRouter);

//readJsonFile();
app.listen(5000, () => {
    console.log("Listening to 5000");
});
