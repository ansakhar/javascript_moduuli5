import User from "../models/user.model.js";
import Request from "../models/request.model.js";
import jwt from "jsonwebtoken";
import dotenv from "dotenv";
import bcrypt, { compare } from "bcrypt";

dotenv.config(); // access environment variables
const jwtKey = process.env.APP_SECRET;
const saltRounds = 10;

// 1. Create a new user: POST /bank/user (name, initial deposit, password) -> user_id
export const postUser = async (req, res) => {
    const user = new User();
    const fields = Object.keys(User.schema.tree);
    //console.log("fields", fields);
    fields.forEach(field => {
        user[field] = req.body[field];
    });
    user.passwordHash = await bcrypt.hash(req.body.password, saltRounds);
    user.account_balance = req.body.init_deposit;
    const savedUser = await user.save();
    res.status(200).json(savedUser._id);
};

// 2. Get account balance: GET /bank/:user_id/balance (id) -> account_balance
export const getUser = async (req, res) => {
    try {
        const user = await User.findById(req.params.user_id);
        if (user !== null) {
            res.status(200).json({account_balance: user.account_balance});
        }
        else res.status(404).json({error: "User not found"});
    } catch (err) {
        res.status(500).send(err);
    }
};

// 7. Login to account: POST /bank/login (id, password) -> token
export const login = async (req, res) => {
    const body = req.body;
    const foundUser = await User.findById(body.id);
    //console.log("foundUser", foundUser);
    if (!foundUser) {
        res.status(404).json({error: "User not found"});
        return;
    }
    const correctPassword = await compare(body.password, foundUser.passwordHash);
    if (!correctPassword) {
        res.status(401).json({error: "Invalid password"});
        return;
    }
    // a new JWT token will be created if username and password are correct:
    const token = jwt.sign({ userId: foundUser._id},
        jwtKey,
        { expiresIn: 60*60 }
    );
    res.status(200).send({token, userId: foundUser._id});
};

// 3. Withdraw money: PATCH /bank/user/withdraw (id, *password, amount) -> new_account_balance
export const withdrawMoney = async (req, res, next) => {
    const body = req.body;
    try {
        const foundUser = await User.findById(body.id);
        if (!foundUser) {
            res.status(404).json({error: "User not found"});
            return;
        }
        if (foundUser.account_balance < body.amount)
            res.status(400).json({error: "Not enough money in the account"});
        else {
            const new_account_balance = foundUser.account_balance - body.amount;
            const updatedUser = await User.findByIdAndUpdate(body.id, { account_balance: new_account_balance }, { new: true });
            res.json({new_account_balance: updatedUser.account_balance});
        }
    } catch(error) {
        next(error);
    }
};

// 4. Deposit money: PATCH /bank/user/deposit (id, *password, amount) -> new_account_balance
export const depositMoney = async (req, res, next) => {
    const body = req.body;
    try {
        const foundUser = await User.findById(body.id);
        //console.log("foundUser", foundUser);
        if (foundUser) {
            const new_account_balance = foundUser.account_balance + body.amount;
            const updatedUser = await User.findByIdAndUpdate(body.id, { account_balance: new_account_balance }, { new: true });
            res.json({new_account_balance: updatedUser.account_balance});
        }
        else res.status(404).json({error: "User not found"});
    } catch(error) {
        next(error);
    }
};

// 5. Transfer money: POST /bank/transfer (id, password, recipient_id, amount) -> new_account_balance
export const transferMoney = async (req, res, next) => {
    const body = req.body;
    try {
        const foundUser = await User.findById(body.id);
        if (!foundUser) {
            res.status(404).json({error: "User not found"});
            return;
        }
        if (foundUser.account_balance < body.amount) {
            res.status(400).json({error: "Not enough money in the account"});
            return;
        }
        const recipient = await User.findById(body.recipient_id);
        if (!recipient) {
            res.status(404).json({error: "Recipient not found"});
            return;
        }
        const new_account_balance = foundUser.account_balance - body.amount;
        const new_recipient_balance = recipient.account_balance + body.amount;
        const updatedUser = await User.findByIdAndUpdate(body.id, { account_balance: new_account_balance }, { new: true });
        await User.findByIdAndUpdate(body.recipient_id, { account_balance: new_recipient_balance }, { new: true });
        res.json({new_account_balance: updatedUser.account_balance});
    } catch(error) {
        next(error);
    }
};

// 6. Update username: PATCH /bank/update (id, new_name) -> new_username
export const updateUsername = async (req, res, next) => {
    const body = req.body;
    try {
        const updatedUser = await User.findByIdAndUpdate(body.id, { name: body.new_name }, { new: true });
        if (updatedUser) {
            res.json({new_name: updatedUser.name});
        }
        else res.status(404).json({error: "User not found"});
    }
    catch(error) {
        next(error);
    }
};

// 6B. Update username: PATCH /bank/user/password (id, new_password) -> new_password
export const updatePassword = async (req, res, next) => {
    const body = req.body;
    try {
        const updatedUser = await User.findByIdAndUpdate(body.id, { password: body.new_password }, { new: true });
        if (updatedUser) {
            res.json({new_password: updatedUser.password});
        }
        else res.status(404).json({error: "User not found"});
    }
    catch(error) {
        next(error);
    }
};

// 8. Request funds: POST /bank/request_funds (target_id, amount) -> request_id
export const requestFunds = async (req, res) => {
    const requestFunds = new Request();
    const fields = Object.keys(Request.schema.tree);
    fields.forEach(field => {
        requestFunds[field] = req.body[field];
    });
    requestFunds.user_id = req.user.userId;
    const savedRequest = await requestFunds.save();
    //console.log("req.user.userId:", req.user.userId);
    res.status(200).json({request_id: savedRequest._id});
};

// 9. Transfer funds by request: POST /bank/transfer_by_request (request_id) -> ?
export const transferByRequest = async (req, res, next) => {
    const body = req.body;
    console.log("req.user:", req.user.userId);
    try {
        const foundRequest = await Request.findById(body.request_id);
        if (!foundRequest) {
            res.status(404).json({error: "Request not found"});
            return;
        }
        console.log("foundRequest:", foundRequest);
        
        if (foundRequest.target_id !== req.user.userId) {
            res.status(400).json({error: "You do not have permission to approve this request"});
            return;
        }
        const foundTarget = await User.findById(foundRequest.target_id);
        if (!foundTarget) {
            res.status(404).json({error: "Target user not found"});
            return;
        }
        if (foundTarget.account_balance < foundRequest.amount) {
            res.status(400).json({error: "Not enough money in the target user's account"});
            return;
        }
        const user = await User.findById(foundRequest.user_id);
        if (!user) {
            res.status(404).json({error: "User not found"});
            return;
        }
        const new_target_balance = foundTarget.account_balance - foundRequest.amount;
        const new_user_balance = user.account_balance + foundRequest.amount;
        await User.findByIdAndUpdate(foundRequest.target_id, { account_balance: new_target_balance }, { new: true });
        await User.findByIdAndUpdate(foundRequest.user_id, { account_balance: new_user_balance }, { new: true });
        res.json("Success!");
    } catch(error) {
        next(error);
    }
};
