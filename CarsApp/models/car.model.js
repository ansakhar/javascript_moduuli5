import mongoose from "mongoose";

const CarSchema = new mongoose.Schema({
    id: Number,
    owner_firstname: String,
    owner_surname: String,
    contact_email: String,
    owner_address: String,
    owner_state: String,
    car_make: String,
    car_model: String,
    car_modelyear: Number,
    price: Number
});

const Car = mongoose.model("users_cars", CarSchema);

export default Car;