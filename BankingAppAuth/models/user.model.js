import mongoose from "mongoose";

const UserSchema = new mongoose.Schema({
    name: String,
    account_balance: Number,
    passwordHash: String
});

const User = mongoose.model("users", UserSchema);

export default User;