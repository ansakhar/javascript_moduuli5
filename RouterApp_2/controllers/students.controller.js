import Student from "../models/students.model.js";

export const getAllStudents = (req, res) => {
    Student.find()
        .then(result => {
            res.send(result);
        })
        .catch(err => {
            res.status(500).send(err);
        });
};

export const getStudent = async (req, res) => {
    try {
        const student = await Student.findById(req.params.id);
        
        res.send(student);
    } catch (err) {
        res.status(500).send(err);
    }
};

export const postStudent = (req, res) => {
    const student = req.body; 

    if(!student.name || !student.class) {
        res.status(400).send("Invalid data");
    } else {
        const newStudent = new Student({name: student.name, class: student.class});
        newStudent.save()
            .then(() => {
                res.status(200).send("New student created");
            })
            .catch(err => {
                res.status(500).send(err);
            });
    }
};

export const putStudent = (req, res) => {
    const student = req.body;
    Student.findByIdAndUpdate(req.params.id, {name: student.name, class: student.class})
        .then(() => {
            res.status(200).send("Student updated");
        })
        .catch(err => {
            res.status(500).send(err);
        });
};

export const deleteStudent = (req, res) => {
    Student.findByIdAndRemove(req.params.id)
        .then(() => {
            res.status(200).send("Student deleted");
        })
        .catch(err => {
            res.status(500).send(err);
        });
};