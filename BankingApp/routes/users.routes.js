import express from "express";
import * as UserController from "../controllers/users.controllers.js";

const router = express.Router();

router.post("/user", UserController.postUser); // 1
router.get("/:user_id/balance", UserController.getUser); // 2
router.patch("/user/withdraw", UserController.withdrawMoney); // 3
router.patch("/user/deposit", UserController.depositMoney); // 4
router.post("/transfer", UserController.transferMoney); // 5
router.patch("/update", UserController.updateUsername); // 6
router.patch("/user/password", UserController.updatePassword); // 6B
/*router.get("/", UserController.getAllUsers);
router.delete("/:id", UserController.deleteUser);*/

export default router;