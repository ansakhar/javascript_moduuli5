import mongoose from "mongoose";

const RequestSchema = new mongoose.Schema({
    amount: Number,
    target_id: String,
    user_id: String
    //status: String
});

const Request = mongoose.model("requests", RequestSchema);

export default Request;