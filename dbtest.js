import mongoose from "mongoose";
import dotenv from "dotenv";

dotenv.config();

async function connectMongoose() {
    try {
        await mongoose.connect(
            "mongodb://localhost/minuntietokanta",
            { 
                useNewUrlParser: true,
                useUnifiedTopology: true,
                user: process.env.MONGODB_USER,
                pass: process.env.MONGODB_PASS
            }
        );
    } catch (err) {
        console.log(err);
    }
}

const ColorSchema = new mongoose.Schema({
    name: String,
    favouriteColor: String
});

connectMongoose();
const FavouriteColor = mongoose.model("FavouriteColors", ColorSchema);

const MikkosColor = new FavouriteColor({
    name: "Mikko",
    favouriteColor: "red"
});

MikkosColor.save()
    .then(result => {
        console.log(result);
        mongoose.connection.close();
    })
    .catch(err => {
        console.log("ERROR", err);
        mongoose.connection.close();
    });