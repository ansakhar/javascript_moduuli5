import mongoose from "mongoose";

const StudentSchema = new mongoose.Schema({
    firstname: String,
    surname: String,
    birthday: Date,
    email: String
});

const Student = mongoose.model("students", StudentSchema);

export default Student;