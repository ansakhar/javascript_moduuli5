import User from "../models/user.model.js";

// 1. Create a new user: POST /bank/user (name, initial deposit, password) -> user_id
export const postUser = async (req, res) => {
    const user = new User();
    const fields = ["name", "init_deposit", "password"];
    fields.forEach(field => {
        user[field] = req.body[field];
    });
    user.account_balance = req.body.init_deposit;
    const savedUser = await user.save();
    res.status(200).json(savedUser._id);
};

// 2. Get account balance: GET /bank/:user_id/balance (id) -> account_balance
export const getUser = async (req, res) => {
    try {
        const user = await User.findById(req.params.user_id);
        if (user !== null) {
            res.status(200).json({account_balance: user.account_balance});
        }
        else res.status(404).json({error: "User not found"});
    } catch (err) {
        res.status(500).send(err);
    }
};

async function findUser(id) {
    return await User.findById(id);
}

// 3. Withdraw money: PATCH /bank/user/withdraw (id, password, amount) -> new_account_balance
export const withdrawMoney = async (req, res, next) => {
    const body = req.body;
    try {
        const foundUser = await findUser(body.id);
        if (foundUser) {
            if(body.password === foundUser.password) {
                if(foundUser.account_balance < body.amount)
                    res.status(400).json({error: "Not enough money in the account"});
                else {
                    const new_account_balance = foundUser.account_balance - body.amount;
                    const updatedUser = await User.findByIdAndUpdate(body.id, { account_balance: new_account_balance }, { new: true });
                    res.json({new_account_balance: updatedUser.account_balance});
                }
            }
            else res.status(401).json({error: "Invalid password"});
        }
        else res.status(404).json({error: "User not found"});
    } catch(error) {
        next(error);
    }
};

// 4. Deposit money: PATCH /bank/user/deposit (id, password, amount) -> new_account_balance
export const depositMoney = async (req, res, next) => {
    const body = req.body;
    try {
        const foundUser = await findUser(body.id);
        //console.log("foundUser", foundUser);
        if (foundUser) {
            if(body.password === foundUser.password) {
                const new_account_balance = foundUser.account_balance + body.amount;
                const updatedUser = await User.findByIdAndUpdate(body.id, { account_balance: new_account_balance }, { new: true });
                res.json({new_account_balance: updatedUser.account_balance});
            }
            else res.status(401).json({error: "Invalid password"});
        }
        else res.status(404).json({error: "User not found"});
    } catch(error) {
        next(error);
    }
};

// 5. Transfer money: POST /bank/transfer (id, password, recipient_id, amount) -> new_account_balance
export const transferMoney = async (req, res, next) => {
    const body = req.body;
    try {
        const foundUser = await findUser(body.id);
        if (foundUser) {
            if(body.password === foundUser.password) {
                if(foundUser.account_balance < body.amount)
                    res.status(400).json({error: "Not enough money in the account"});
                else {
                    const recipient = await findUser(body.recipient_id);
                    if (recipient) {
                        const new_account_balance = foundUser.account_balance - body.amount;
                        const new_recipient_balance = recipient.account_balance + body.amount;
                        const updatedUser = await User.findByIdAndUpdate(body.id, { account_balance: new_account_balance }, { new: true });
                        await User.findByIdAndUpdate(body.recipient_id, { account_balance: new_recipient_balance }, { new: true });
                        res.json({new_account_balance: updatedUser.account_balance});
                    }
                    else res.status(404).json({error: "Recipient not found"});
                }
            }
            else res.status(401).json({error: "Invalid password"});
        }
        else res.status(404).json({error: "User not found"});
    } catch(error) {
        next(error);
    }
};

// 6. Update username: PATCH /bank/update (id, new_name) -> new_username
export const updateUsername = async (req, res, next) => {
    const body = req.body;
    try {
        const updatedUser = await User.findByIdAndUpdate(body.id, { name: body.new_name }, { new: true });
        if (updatedUser) {
            res.json({new_name: updatedUser.name});
        }
        else res.status(404).json({error: "User not found"});
    }
    catch(error) {
        next(error);
    }
};

// 6B. Update username: PATCH /bank/user/password (id, new_password) -> new_password
export const updatePassword = async (req, res, next) => {
    const body = req.body;
    try {
        const updatedUser = await User.findByIdAndUpdate(body.id, { password: body.new_password }, { new: true });
        if (updatedUser) {
            res.json({new_password: updatedUser.password});
        }
        else res.status(404).json({error: "User not found"});
    }
    catch(error) {
        next(error);
    }
};

/*
export const getAllUsers = async (request, response) => {
    const users = await User.find({});
    response.json(users);
};

export const putUser = async (req, res, next) => {
    const body = req.body;
    try {
        const updatedUser = await User.findByIdAndUpdate(req.params.id, body, { new: true });
        if (updatedUser) res.json(updatedUser);
        else res.status(404).json({error: "User not found"});
    } catch(error) {
        next(error);
    }
};

export const deleteUser = async (req, res) => {
    const user = await User.findOneAndDelete({id:req.params.id});
    console.log("user", user);
    if (user !== null) res.json(user);
    else res.status(404).json({error: "User not found"});
};*/
