import mongoose from "mongoose";
import dotenv from "dotenv";

dotenv.config();

/*async function connectMongoose() {
    try {
        await mongoose.connect(process.env.MONGODB_URI, {useNewUrlParser: true, useUnifiedTopology: true});
    } catch (err) {
        console.log(err);
    }
}*/

mongoose.connect(process.env.MONGODB_URI, {useNewUrlParser: true, useUnifiedTopology: true});

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", function() {
    console.log("Database connected");
});

const StudentSchema = new mongoose.Schema({
    firstname: String,
    surname: String,
    birthday: Date,
    email: String
});

//connectMongoose();
const Student = mongoose.model("students", StudentSchema);

const anyStudent = new Student({
    firstname: "test",
    surname: "sannasurname",
    birthday: "1999-06-12",
    email: "anna@email.fi"
});


anyStudent.save()
    .then(result => {
        console.log(result);
        mongoose.connection.close();
    })
    .catch(err => {
        console.log("ERROR", err);
        mongoose.connection.close();
    });