import express from "express";
import * as StudentController from "../controllers/students.controllers.js";

const router = express.Router();

// Student includes id, email and name
router.get("/", StudentController.getAllStudents);
router.get("/:id", StudentController.getStudent);
router.post("/", StudentController.postStudent);
router.put("/:id", StudentController.putStudent);
router.delete("/:id", StudentController.deleteStudent);

export default router;