### 1. Create a new user:

POST http://localhost:5000/bank/user HTTP/1.1
content-type: application/json

{
    "name": "second",
    "init_deposit": 100,
    "password": "passfith"
}


### 2. Get account balance:

GET http://localhost:5000/bank/6229e635da82f136ed0e98b3/balance HTTP/1.1
content-type: application/json


### 7. Login to account, token generation:

POST http://localhost:5000/bank/login HTTP/1.1
content-type: application/json

{
    "id": "6229e62dda82f136ed0e98b0",
    "password": "passfith"
}


### 3. Withdraw money:

PATCH http://localhost:5000/bank/user/withdraw HTTP/1.1
content-type: application/json
Authorization: bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MjI4OGY1YWFmNzczYmZiMjBiMTk1NzgiLCJpYXQiOjE2NDY5MTAyMjIsImV4cCI6MTY0NjkxMzgyMn0.jSn5VWyUeh0ksXf2SZhfHn-52HUMSDF_j522pgKz2HE

{
    "id": "62288f5aaf773bfb20b19578",
    "amount": 10
}

### 4. Deposit money:

PATCH http://localhost:5000/bank/user/deposit HTTP/1.1
content-type: application/json
Authorization: bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MjI4OGY1YWFmNzczYmZiMjBiMTk1NzgiLCJpYXQiOjE2NDY5MTEwNDgsImV4cCI6MTY0NjkxNDY0OH0.KmvrfaowMlJCaJzb7aob4UrmByT0awgQGAbaMoQQ_Nc

{
    "id": "62288f5aaf773bfb20b19578",
    "amount": 10
}

### 5. Transfer money:

POST http://localhost:5000/bank/transfer HTTP/1.1
content-type: application/json
Authorization: bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MjI5ZTYyZGRhODJmMTM2ZWQwZTk4YjAiLCJpYXQiOjE2NDY5MTMxOTcsImV4cCI6MTY0NjkxNjc5N30.keA57meL03_U0ZAjym_zsxxSpn_aTn8WyoQRQY66PD4

{
    "id": "6229e62dda82f136ed0e98b0",
    "recipient_id": "6229e635da82f136ed0e98b3",
    "amount": 10
}


### 6. Update username:

PATCH http://localhost:5000/bank/update HTTP/1.1
content-type: application/json
Authorization: bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MjI4OGY1YWFmNzczYmZiMjBiMTk1NzgiLCJpYXQiOjE2NDY4OTg5NDksImV4cCI6MTY0NjkwMjU0OX0.fLItvqmDTlFG6CwiiQkaqw5srUx5Kxlzh6y0I3NN2bU

{
    "id": "62288f5aaf773bfb20b19578",
    "new_name": "name updated"
}


### 6B. Update password:

PATCH http://localhost:5000/bank/user/password HTTP/1.1
content-type: application/json
Authorization: bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MjI4OGY1YWFmNzczYmZiMjBiMTk1NzgiLCJpYXQiOjE2NDY4OTg5NDksImV4cCI6MTY0NjkwMjU0OX0.fLItvqmDTlFG6CwiiQkaqw5srUx5Kxlzh6y0I3NN2bU

{
    "id": "62288f5aaf773bfb20b19578",
    "new_password": "passsecond new"
}

### 8. Request funds:

POST http://localhost:5000/bank/request_funds HTTP/1.1
content-type: application/json
Authorization: bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MjI5ZTYyZGRhODJmMTM2ZWQwZTk4YjAiLCJpYXQiOjE2NDcwMDQ5ODEsImV4cCI6MTY0NzAwODU4MX0.5Kjuw3HNfSiHOFbikIEpCpw404azhRrviZKJ854Pd7U

{
    "target_id": "6229e635da82f136ed0e98b3",
    "amount": 12
}

### 9. Transfer money by request:

POST http://localhost:5000/bank/transfer_by_request HTTP/1.1
content-type: application/json
Authorization: bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MjI5ZTYyZGRhODJmMTM2ZWQwZTk4YjAiLCJpYXQiOjE2NDcwMDEwMTAsImV4cCI6MTY0NzAwNDYxMH0.7odQZ42_6YPpMZh38427X7ZyLhO7mz7oAMxcJX-hB0c

{
    "request_id": "622b353fdb05d53585b0c782"
}

### 10. Multiple request funds: ei ole valmis...

POST http://localhost:5000/bank/request_funds HTTP/1.1
content-type: application/json
Authorization: bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2MjI5ZTYyZGRhODJmMTM2ZWQwZTk4YjAiLCJpYXQiOjE2NDY5OTg2MjUsImV4cCI6MTY0NzAwMjIyNX0.dt2V4KyTEbAxsuHYcQ2IAwUT87GbcPaWq1rniI7K30o

{
    "target_id": "6229e635da82f136ed0e98b3",
    "amount": 12
}