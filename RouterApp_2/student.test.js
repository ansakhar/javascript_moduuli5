import request from "supertest";
import {app} from "./studentrest.js";

describe("Student api works", () => {

    it("Adds new students when POST is used", async () => {
        return await request(app)
            .post("/students")
            .send({name: "Mikko Mallikas", class: "3B"})
            .expect(200)
            .expect("New student created");
    });

    it("Doesn't add a new student if name is missing", async () => {
        return await request(app)
            .post("/students")
            .send({name: "", class: "3B"})
            .expect(400)
            .expect("Invalid data");
    });

    it("Doesn't add a new student if class is missing", async () => {
        return await request(app)
            .post("/students")
            .send({name: "Mikko Mallikas", class: ""})
            .expect(400)
            .expect("Invalid data");
    });

    it("can get all students", async () => {
        const response = await request(app)
            .get("/students")
            .expect(200);
        
        expect(response.body[0].name).toBe("Mikko Mallikas");
        expect(response.body[0].class).toBe("3B");
        expect(response.body[0].id).toBeDefined();
    });

    it("can get a student by ID", () => {

    });

    it("can update a student by ID", () => {

    });

    it("can delete a student by ID", () => {

    });
});